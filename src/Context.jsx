import React, { useEffect, useState } from "react";

export const AppContext = React.createContext();

export function AppContextProvider({ children }) {
  const [currentCity, setCurrentCity] = useState(
    () => JSON.parse(localStorage.getItem("current-city")) || ""
  );
  const [openModal, setOpenModal] = useState(currentCity ? false : true);
  const [signInModal, setSignInModal] = useState(false);
  const [movieData, setMovieData] = useState([]);
  const [cityMovieData, setCityMovieData] = useState(
    () => JSON.parse(localStorage.getItem("city-movie")) || []
  );
  const [currentUser, setCurrentUser] = useState(
    () => localStorage.getItem("current-user") || ""
  );
  const [currentUserData, setCurrentUserData] = useState({});
  const [drawer, setDrawer] = useState(false);
  const [openEmailModal, setOpenEmailModal] = useState(false);
  const [openPhoneModal, setOpenPhoneModal] = useState(false);
  const [seatModal, setSeatModal] = useState(() =>
    JSON.parse(localStorage.getItem("seats"))?.seats > 0 ? false : true
  );

  useEffect(() => {
    const URL = "https://localhost:7285/Movie/GetMovies";
    fetch(URL)
      .then((res) => res.json())
      .then((data) => setMovieData(data))
      .catch((err) => console.log(err));
  }, []);

  useEffect(() => {
    localStorage.setItem("current-city", JSON.stringify(currentCity));
    const URL = `https://localhost:7285/City/GetCity?name=${currentCity}`;
    fetch(URL)
      .then((res) => res.json())
      .then((data) => {
        const idArr = data.movieIds?.split(",").map((item) => +item);
        const newMovies = movieData.filter((item) => idArr.includes(item.id));
        setCityMovieData((prev) => (newMovies.length ? newMovies : prev));
        localStorage.setItem(
          "city-movie",
          JSON.stringify(newMovies.length ? newMovies : cityMovieData)
        );
      })
      .catch((err) => console.log(err));
  }, [currentCity]);

  useEffect(() => {
    const URL = `https://localhost:7285/User/GetUser?phone=${currentUser}`;
    fetch(URL)
      .then((res) => res.json())
      .then((data) => setCurrentUserData(data))
      .catch((err) => console.log());
  }, [currentUser]);

  return (
    <AppContext.Provider
      value={{
        openModal,
        setOpenModal,
        currentCity,
        setCurrentCity,
        movieData,
        cityMovieData,
        signInModal,
        setSignInModal,
        currentUser,
        setCurrentUser,
        currentUserData,
        setCurrentUserData,
        drawer,
        setDrawer,
        openEmailModal,
        setOpenEmailModal,
        openPhoneModal,
        setOpenPhoneModal,
        seatModal,
        setSeatModal,
      }}
    >
      {children}
    </AppContext.Provider>
  );
}
