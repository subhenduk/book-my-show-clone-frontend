import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { BsSearch } from "react-icons/bs";
import { RxTriangleDown } from "react-icons/rx";
import { AiOutlineMenu } from "react-icons/ai";

import { AppContext } from "../Context";

export default function Header() {
  const {
    setOpenModal,
    currentCity,
    setSignInModal,
    currentUser,
    currentUserData,
    setDrawer,
  } = useContext(AppContext);

  return (
    <div className="h-16 bg-slate-700">
      <div className="w-10/12 h-full mx-auto flex justify-between items-center">
        {/* Left Part */}
        <div className="flex items-center gap-x-4">
          <h1 className="font-bold text-lg text-white cursor-pointer">
            <Link to="/">BookMyMovie</Link>
          </h1>
          <div className="relative">
            <i className="absolute top-2 left-2">
              <BsSearch />
            </i>
            <input
              type="text"
              placeholder="&nbsp;Search for Movies, Events, Plays, Sports and Activities"
              className="w-[32rem] h-8 rounded-sm outline-none pl-8 text-xs"
            />
          </div>
        </div>

        {/* Right Part */}
        <div>
          <div className="flex gap-x-4 items-center">
            <div
              onClick={() => setOpenModal((prev) => !prev)}
              className="flex gap-x-1 items-center text-slate-200 hover:text-white transition-colors cursor-pointer"
            >
              <h1>{currentCity || "Select City"}</h1>
              <i>
                <RxTriangleDown />
              </i>
            </div>

            {currentUser ? (
              <h1
                className="text-sm font-bold text-white cursor-pointer"
                onClick={() => setDrawer(true)}
              >
                Hello,{" "}
                {currentUserData.firstName
                  ? `${currentUserData.firstName}`
                  : "Guest"}
              </h1>
            ) : (
              <div className="flex items-center gap-x-2">
                <button
                  className="bg-rose-500 text-xs font-semibold text-white px-4 py-1 rounded-sm hover:bg-rose-800 transition-colors"
                  onClick={() => setSignInModal(true)}
                >
                  Sign In
                </button>
                <i
                  className="text-white text-xl font-bold cursor-pointer mb-1"
                  onClick={() => setDrawer(true)}
                >
                  <AiOutlineMenu />
                </i>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
