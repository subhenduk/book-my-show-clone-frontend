import React from "react";

export default function Navbar() {
  const leftDiv = [
    "Movies",
    "Stream",
    "Events",
    "Plays",
    "Sports",
    "Activities",
    "Buzz",
  ];
  const rightDiv = ["List Your Show", "Corporates", "Offers", "Gift Cards"];

  return (
    <div className="h-8 bg-slate-800">
      <div className="w-10/12 h-full mx-auto text-xs text-white flex justify-between">
        {/* left div */}
        <div className="h-full flex items-center gap-x-4">
          {leftDiv.map((item, i) => (
            <p key={i} className="cursor-pointer hover:underline">
              {item}
            </p>
          ))}
        </div>

        {/* right div */}
        <div className="h-full flex items-center gap-x-4">
          {rightDiv.map((item, i) => (
            <p key={i} className="cursor-pointer hover:underline">
              {item}
            </p>
          ))}
        </div>
      </div>
    </div>
  );
}
