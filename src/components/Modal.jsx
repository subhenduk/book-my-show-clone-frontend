import React, { useContext } from "react";
import { BsSearch } from "react-icons/bs";
import { AppContext } from "../Context";

export default function Modal() {
  const { openModal, setOpenModal, currentCity, setCurrentCity } =
    useContext(AppContext);

  const popularCities = [
    {
      id: 1,
      name: "Mumbai",
      icon: "https://in.bmscdn.com/m6/images/common-modules/regions/mumbai.png",
      iconSelected:
        "https://in.bmscdn.com/m6/images/common-modules/regions/mumbai-selected.png",
    },
    {
      id: 2,
      name: "Delhi-NCR",
      icon: "https://in.bmscdn.com/m6/images/common-modules/regions/ncr.png",
      iconSelected:
        "https://in.bmscdn.com/m6/images/common-modules/regions/ncr-selected.png",
    },
    {
      id: 3,
      name: "Bengaluru",
      icon: "https://in.bmscdn.com/m6/images/common-modules/regions/bang.png",
      iconSelected:
        "https://in.bmscdn.com/m6/images/common-modules/regions/bang-selected.png",
    },
    {
      id: 4,
      name: "Hyderabad",
      icon: "https://in.bmscdn.com/m6/images/common-modules/regions/hyd.png",
      iconSelected:
        "https://in.bmscdn.com/m6/images/common-modules/regions/hyd-selected.png",
    },
    {
      id: 5,
      name: "Ahmedabad",
      icon: "https://in.bmscdn.com/m6/images/common-modules/regions/ahd.png",
      iconSelected:
        "https://in.bmscdn.com/m6/images/common-modules/regions/ahd-selected.png",
    },
    {
      id: 6,
      name: "Chandigarh",
      icon: "https://in.bmscdn.com/m6/images/common-modules/regions/chd.png",
      iconSelected:
        "https://in.bmscdn.com/m6/images/common-modules/regions/chd-selected.png",
    },
    {
      id: 7,
      name: "Chennai",
      icon: "https://in.bmscdn.com/m6/images/common-modules/regions/chen.png",
      iconSelected:
        "https://in.bmscdn.com/m6/images/common-modules/regions/chen-selected.png",
    },
    {
      id: 8,
      name: "Pune",
      icon: "https://in.bmscdn.com/m6/images/common-modules/regions/pune.png",
      iconSelected:
        "https://in.bmscdn.com/m6/images/common-modules/regions/pune-selected.png",
    },
    {
      id: 9,
      name: "Kolkata",
      icon: "https://in.bmscdn.com/m6/images/common-modules/regions/kolk.png",
      iconSelected:
        "https://in.bmscdn.com/m6/images/common-modules/regions/kolk-selected.png",
    },
    {
      id: 10,
      name: "Kochi",
      icon: "https://in.bmscdn.com/m6/images/common-modules/regions/koch.png",
      iconSelected:
        "https://in.bmscdn.com/m6/images/common-modules/regions/koch-selected.png",
    },
  ];

  return (
    <>
      {openModal && (
        <div>
          <div
            style={{ zIndex: 20 }}
            className="fixed top-0 w-screen h-screen bg-opacity-40 bg-black transition-colors z-70"
            onClick={() => setOpenModal(false)}
          ></div>
          {/* actual Modal */}
          <div
            style={{ zIndex: 30 }}
            className="fixed top-16 left-60 w-8/12 bg-white shadow-lg rounded-md z-80 px-8 py-6"
          >
            {/* Top Searchbar */}
            <div className="relative">
              <input
                type="text"
                placeholder="&nbsp;Search for your city"
                autoFocus
                className="w-full h-10 outline-none shadow-md border-1 rounded-md px-4 py-2 text-sm pl-10"
              />
              <i className="absolute top-3 left-3">
                <BsSearch />
              </i>
            </div>

            {/* Popular cities */}
            <div className="mt-10">
              <h1 className="text-center mb-6 font-bold text-sm">
                Popular Cities
              </h1>
              <div className="flex justify-between cursor-pointer">
                {popularCities.map((item) => (
                  <div
                    key={item.id}
                    className="flex flex-col justiy-center items-center"
                    onClick={() => {
                      setCurrentCity(item.name);
                      setOpenModal(false);
                    }}
                  >
                    <img
                      src={
                        currentCity === item.name
                          ? item.iconSelected
                          : item.icon
                      }
                      alt=""
                    />
                    <h1 className="text-sm">{item.name}</h1>
                  </div>
                ))}
              </div>
              <h1 className="text-center mt-4 text-red-500 cursor-pointer font-bold hover:underline">
                View all cities
              </h1>
            </div>
          </div>
        </div>
      )}
    </>
  );
}
