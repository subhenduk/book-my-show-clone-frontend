import React, { useContext, useEffect, useState } from "react";
import { AppContext } from "../Context";
import clsx from "clsx";

export default function SeatModal() {
  const data = [
    {
      id: 1,
      seats: 1,
      name: "Cycle",
      img: "/svgs/cycle.png",
    },
    {
      id: 2,
      seats: 2,
      name: "Scooter",
      img: "/svgs/scooter.png",
    },
    {
      id: 3,
      seats: 3,
      name: "Auto Rikshaw",
      img: "/svgs/auto.png",
    },
    {
      id: 4,
      seats: 4,
      name: "Car",
      img: "/svgs/car.png",
    },
    {
      id: 5,
      seats: 5,
      name: "Sedan",
      img: "/svgs/sedan.png",
    },
    {
      id: 6,
      seats: 6,
      name: "Sedan",
      img: "/svgs/sedan.png",
    },
    {
      id: 7,
      seats: 7,
      name: "Sedan",
      img: "/svgs/sedan.png",
    },
    {
      id: 8,
      seats: 8,
      name: "Bus",
      img: "/svgs/bus.png",
    },
    {
      id: 9,
      seats: 9,
      name: "Bus",
      img: "/svgs/bus.png",
    },
    {
      id: 10,
      seats: 10,
      name: "Bus",
      img: "/svgs/bus.png",
    },
  ];
  const [state, setState] = useState(
    () => JSON.parse(localStorage.getItem("seats")) || data[1]
  );
  const [hoverState, setHoverState] = useState(state);
  const { seatModal, setSeatModal } = useContext(AppContext);

  const nums = Array.from({ length: 10 }, (_, i) => i + 1);

  useEffect(() => {
    localStorage.setItem("seats", JSON.stringify(state));
  }, [state]);

  return (
    <>
      {seatModal && (
        <div className="w-screen relative">
          {/* Actuall Modal */}
          <div
            style={{ zIndex: 20 }}
            className="absolute left-[34rem] top-10 w-4/12 bg-white shadow-lg rounded-md z-80 px-8 py-6"
          >
            <div className="flex flex-col items-center gap-y-5">
              <h1 className="text-center text-md">How Many Seats ?</h1>
              <img
                src={hoverState.img || state.img}
                alt={hoverState.name || state.name}
                className="w-48 h-40"
              />
              <div className="flex justify-center items-cetner gap-x-6 text-sm ">
                {nums.map((item) => (
                  <div
                    key={item}
                    onMouseEnter={() =>
                      setHoverState(data.find((d) => d.seats === item))
                    }
                    onMouseLeave={() => setHoverState({})}
                    onClick={() => setState(data.find((d) => d.seats === item))}
                    className={clsx(
                      "w-6 h-6 rounded-full flex items-center justify-center text-xs  cursor-pointer transition-colors",
                      (item === hoverState.seats || item === state.seats) &&
                        "bg-rose-500 text-white"
                    )}
                  >
                    {item}
                  </div>
                ))}
              </div>
              <button
                onClick={() => {
                  setSeatModal(false);
                  location.reload();
                }}
                className="text-md text-white rounded-md bg-rose-500 w-full py-2 hover:bg-rose-900 mt-5"
              >
                Select Seats
              </button>
            </div>
          </div>

          {/* Shadow Screen */}
          <div
            style={{ zIndex: 10 }}
            className="fixed top-0 w-full h-screen bg-opacity-40 bg-black transition-colors z-70"
            onClick={() => setSeatModal(false)}
          ></div>
        </div>
      )}
    </>
  );
}
