import React from "react";

export default function EditNav() {
  const items = [
    "Your Orders",
    "Stream Library",
    "QuikPay",
    "Reward",
    "Restaurant Discounts",
    "Discount Store",
    "Profile",
    "Saved Devices",
  ];
  return (
    <div className="h-10 bg-slate-900 w-full">
      <div className="w-10/12 h-full mx-auto flex items-center text-white text-xs gap-x-6 cursor-pointer px-4">
        {items.map((item, id) => (
          <div key={id}>{item}</div>
        ))}
      </div>
    </div>
  );
}
