import React, { useContext, useEffect, useState } from "react";
import { AppContext } from "../Context";
// Icons
import { FcGoogle } from "react-icons/Fc";
import { AiOutlineMail, AiFillApple } from "react-icons/ai";
import { RxCross2 } from "react-icons/rx";

export default function SignIn() {
  const [userNumber, setUserNumber] = useState("");
  const [submit, setSubmit] = useState(false);

  const { signInModal, setSignInModal, setCurrentUser } =
    useContext(AppContext);

  useEffect(() => {
    const URL = `https://localhost:7285/User/PostUser?phone=${userNumber}`;
    if (submit) {
      fetch(URL, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
      });
      localStorage.setItem("current-user", userNumber);
      setCurrentUser(userNumber);
      setSignInModal(false);
      setUserNumber("");
      setSubmit(false);
      location.reload();
    }
  }, [submit]);

  function handleChange(e) {
    setUserNumber(e.target.value);
  }

  function handleSubmit() {
    setSubmit(true);
  }

  return (
    <>
      {signInModal && (
        <div>
          <div
            style={{ zIndex: 20 }}
            className="fixed top-0 w-screen h-screen bg-opacity-40 bg-black transition-colors z-70"
          ></div>
          <div
            style={{ zIndex: 30 }}
            className="fixed top-16 left-[32rem] w-4/12 bg-white shadow-lg rounded-md z-80 px-8 py-6"
          >
            <div className="flex items-center justify-center relative">
              <h1 className="text-md font-bold">Get Started</h1>
              <i
                className="hover:text-red-500 cursor-pointer absolute right-0 text-bold"
                onClick={() => setSignInModal(false)}
              >
                <RxCross2 />
              </i>
            </div>

            <div className="flex flex-col gap-y-6 mt-10 mx-8">
              <button className="border-2 flex justify-between px-8 py-2 mx-10 hover:bg-slate-200 transition-all">
                <i>
                  <FcGoogle size="22" />
                </i>
                <h1>Continue with Google</h1>
              </button>

              <button className="border-2 flex justify-between mx-10 px-8 py-2 hover:bg-slate-200 transition-all">
                <i>
                  <AiOutlineMail size="22" />
                </i>
                <h1>Continue with Email&nbsp;&nbsp;&nbsp;</h1>
              </button>

              <button className="border-2 flex justify-between mx-10 px-8 py-2 hover:bg-slate-200 transition-all">
                <i>
                  <AiFillApple size="22" />
                </i>
                <h1>Continue with Apple&nbsp;&nbsp;</h1>
              </button>
            </div>

            <div className="mt-10 mb-5">
              {/* Name Field */}
              <div className="flex items-center justify-center">
                <img
                  className="mt-2"
                  src="https://in.bmscdn.com/webin/common/icons/indianflag.svg"
                  alt=""
                />
                <h1 className="mt-2 mx-2">+91</h1>
                <input
                  id="name"
                  type="text"
                  autoComplete="off"
                  className="mt-2 h-8 outline-none border-0 border-b-2 border-blue-200 focus:border-blue-800 py-2 transition-colors"
                  name="name"
                  value={userNumber}
                  onChange={handleChange}
                />
              </div>

              {/* login and submit */}
              <div className="flex justify-end items-center mx-6 mt-4">
                <button
                  className="px-4 py-2 bg-rose-600 text-white rounded-md mt-4 disabled:bg-slate-400 disabled:cursor-not-allowed"
                  onClick={handleSubmit}
                  disabled={userNumber.length == 10 ? false : true}
                >
                  Register
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
}
