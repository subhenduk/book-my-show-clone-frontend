import React, { useContext, useEffect, useState } from "react";
import { AppContext } from "../Context";
// Icons
import { BsChevronLeft } from "react-icons/bs";

export default function EmailModal() {
  const {
    openPhoneModal,
    setOpenPhoneModal,
    currentUserData,
    setCurrentUserData,
  } = useContext(AppContext);
  const [submit, setSubmit] = useState(false);
  const [res, setRes] = useState(true);

  useEffect(() => {
    const URL = `https://localhost:7285/User/PutPhone?id=${
      currentUserData.id
    }&phone=${encodeURIComponent(currentUserData.mobile)}`;

    const reqOptions = {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
    };

    if (submit) {
      fetch(URL, reqOptions)
        .then((res) => res.json())
        .then((data) => {
          setRes(data);
          if (data) {
            setOpenPhoneModal(false);
            localStorage.setItem("current-user", currentUserData.mobile);
          }
        })
        .then(() => setSubmit(false))
        .catch((err) => console.log(err));
    }
  }, [submit]);

  function handleSubmit() {
    setSubmit(true);
  }

  return (
    <>
      {openPhoneModal && (
        <div className="z-90">
          {/* Black screen */}
          <div className="fixed top-0 w-screen h-screen bg-opacity-40 bg-black transition-colors z-70"></div>
          {/* Modal */}
          <div className="fixed top-56 left-[36rem] w-3/12 bg-white shadow-lg rounded-md px-8 py-6">
            <div className="flex flex-col gap-y-6">
              <i onClick={() => setOpenPhoneModal(false)}>
                <BsChevronLeft className="hover:text-red-500 cursor-pointer text-bold" />
              </i>
              <h1 className="text-md font-bold ml-6">Edit Your Phone Number</h1>
            </div>

            <div className="mt-2 mb-5 px-4">
              {/* Name Field */}
              <div className="flex flex-col justify-center">
                <h1 className="mt-2 mx-2 text-xs">
                  Enter or Edit a valid Phone Number below
                </h1>
                <input
                  type="text"
                  name="email"
                  value={currentUserData.mobile}
                  onChange={(e) =>
                    setCurrentUserData((prev) => ({
                      ...prev,
                      mobile: e.target.value,
                    }))
                  }
                  autoComplete="off"
                  className="mt-4 outline-none border-2 border-rose-300 focus:border-rose-600 rounded-md mx-2 p-2 transition-colors"
                />
                {!res && (
                  <h1 className="mt-2 text-xs text-red-500 text-center">
                    phone number already exists
                  </h1>
                )}
              </div>

              {/* login and submit */}
              <div className="flex justify-end items-center mx-2">
                <button
                  className="px-4 py-2 bg-rose-600 text-white rounded-md mt-4 disabled:bg-slate-400 disabled:cursor-not-allowed"
                  onClick={handleSubmit}
                >
                  Verify
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
}
