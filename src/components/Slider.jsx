import React from "react";
import SimpleImageSlider from "react-simple-image-slider";

export default function Slider() {
  const images = [
    {
      id: 1,
      url: "https://assets-in.bmscdn.com/promotions/cms/creatives/1682617779318_webbannernpa.jpg",
    },
    {
      id: 2,
      url: "https://assets-in.bmscdn.com/promotions/cms/creatives/1683108543504_giftcardweb.jpg",
    },
    {
      id: 3,
      url: "https://assets-in.bmscdn.com/promotions/cms/creatives/1684229973024_summeractivitiesdesktop.jpg",
    },
    {
      id: 4,
      url: "https://assets-in.bmscdn.com/promotions/cms/creatives/1684835647535_thegiantwheelfestivaldesktop.jpg",
    },
    {
      id: 5,
      url: "https://assets-in.bmscdn.com/promotions/cms/creatives/1680083241797_rewardsweb.jpg",
    },
  ];
  return (
    <div style={{ zIndex: -1 }}>
      <SimpleImageSlider
        width={1537}
        height={307}
        images={images}
        showBullets={true}
        showNavs={false}
        loop={true}
        autoPlay={true}
        slideDuration={0.75}
      />
    </div>
  );
}
