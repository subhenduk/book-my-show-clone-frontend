import React, { useContext } from "react";
import { AppContext } from "../Context";
import { Link } from "react-router-dom";

export default function Recommended() {
  const { movieData, cityMovieData, currentCity } = useContext(AppContext);
  return (
    <div className="w-11/12 mx-auto">
      <h1 className="text-xl font-bold mt-4 ml-4">Recommended Movies</h1>
      {(currentCity ? cityMovieData : movieData).map((item) => (
        <Link key={item.id} to={`/movie/${item.id}`}>
          <div className="w-56 inline-block p-4 cursor-pointer">
            <img src={item.cover} alt="Movie img" className="rounded-md" />
            <h2 className="text-md font-bold mt-2">{item.name}</h2>
            <h2 className="text-xs">{item.genres}</h2>
          </div>
        </Link>
      ))}
    </div>
  );
}
