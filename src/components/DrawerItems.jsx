import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { AppContext } from "../Context";
import {
  BsBell,
  BsShopWindow,
  BsCollectionPlay,
  BsCreditCard2Back,
  BsChatDots,
  BsGear,
  BsGift,
  BsCashCoin,
  BsBagCheck,
  BsEmojiSmile,
  BsChevronRight,
} from "react-icons/bs";

export default function DrawerItems() {
  const items = [
    {
      id: 1,
      name: "Notifications",
      about: "",
      icon: <BsBell />,
    },
    {
      id: 2,
      name: "Your Orders",
      about: "View all your bookings & purchases",
      icon: <BsShopWindow />,
    },
    {
      id: 3,
      name: "Stream Library",
      about: "Rented & Purchased Movies",
      icon: <BsCollectionPlay />,
    },
    {
      id: 4,
      name: "Play Credit Card",
      about: "View your Play Credit Card details & offers",
      icon: <BsCreditCard2Back />,
    },
    {
      id: 5,
      name: "Help & Support",
      about: "View commonly asked Queries & Chat",
      icon: <BsChatDots />,
    },
    {
      id: 6,
      name: "Accounts & Settings",
      about: "Location, Payments, Permissions & More",
      icon: <BsGear />,
    },
    {
      id: 7,
      name: "Rewards",
      about: "View your rewards & unlock new ones",
      icon: <BsGift />,
    },
    {
      id: 8,
      name: "Restaurant Discounts",
      icon: <BsCashCoin />,
    },
    {
      id: 9,
      name: "Discount Store",
      icon: <BsBagCheck />,
    },
    {
      id: 10,
      name: "BookASmile",
      icon: <BsEmojiSmile />,
    },
  ];

  const { currentUserData, setDrawer } = useContext(AppContext);

  return (
    <div className="overflow-auto">
      <div className="bg-slate-800 p-6">
        <h1 className="text-xl text-white font-bold">
          Hi,{" "}
          {currentUserData.firstName && currentUserData.lastName
            ? `${currentUserData.firstName} ${currentUserData.lastName}`
            : "Guest"}
        </h1>
        <h3 className="text-sm text-slate-300 hover:underline cursor-pointer flex">
          <Link to="/edit" onClick={() => setDrawer(false)}>
            Edit Profile
          </Link>
          <i className="mt-1 ml-1">
            <BsChevronRight />
          </i>
        </h3>
      </div>
      {items.map((item, id) => (
        <div
          key={item.id}
          className="flex items-center justify-between p-4 border-b-2 hover:bg-slate-100 cursor-pointer"
        >
          <div className="flex items-center ">
            {item.icon}
            <h1 className="text-md ml-4">{item.name}</h1>
          </div>
          <BsChevronRight />
        </div>
      ))}
      <div className="flex p-2">
        <button
          className="border-2 border-red-600 w-full h-10 font-bold text-red-600 hover:bg-red-100"
          onClick={() => {
            localStorage.removeItem("current-user");
            location.reload();
          }}
        >
          Sign Out
        </button>
      </div>
    </div>
  );
}
