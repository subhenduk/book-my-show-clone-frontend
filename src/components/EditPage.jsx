import React, { useState, useEffect, useContext } from "react";
// icons
import { AiOutlineSave } from "react-icons/ai";
import { BsCamera } from "react-icons/bs";
import { FiEdit } from "react-icons/fi";
// Context
import { AppContext } from "../Context";

import EmailModal from "./EmailModal";
import PhoneModal from "./PhoneModal";

export default function EditPage() {
  const {
    currentUserData,
    setCurrentUserData,
    openEmailModal,
    setOpenEmailModal,
    openPhoneModal,
    setOpenPhoneModal,
  } = useContext(AppContext);
  const [change, setChange] = useState(false);
  const [update, setUpdate] = useState(false);
  const [ageError, setAgeError] = useState(false);
  const [anniversaryError, setAnniversaryError] = useState(false);
  const [states, setStates] = useState([]);
  const [dists, setDists] = useState([]);

  useEffect(() => {
    const URL =
      "https://raw.githubusercontent.com/sab99r/Indian-States-And-Districts/master/states-and-districts.json";
    fetch(URL)
      .then((res) => res.json())
      .then((data) => setStates(data.states))
      .catch((err) => console.log(err));
  }, []);

  useEffect(() => {
    const URL =
      "https://raw.githubusercontent.com/sab99r/Indian-States-And-Districts/master/states-and-districts.json";
    fetch(URL)
      .then((res) => res.json())
      .then((data) =>
        setDists(
          data.states.filter((item) => item.state === currentUserData.state)[0]
            ?.districts
        )
      );
  }, [currentUserData.state]);

  useEffect(() => {
    if (update) {
      const URL = "https://localhost:7285/User/PutUser";
      const reqOptions = {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(currentUserData),
      };

      fetch(URL, reqOptions)
        .then((data) => console.log(data))
        .catch((err) => console.log(err));

      location.reload();
    }
  }, [update]);

  function calculateAge(str) {
    var dob = new Date(str);
    var month_diff = Date.now() - dob.getTime();
    var age_dt = new Date(month_diff);
    var year = age_dt.getUTCFullYear();
    var age = Math.abs(year - 1970);
    return age;
  }

  function calculateAnniversary(str) {
    var month_diff =
      new Date(str).getTime() - new Date(currentUserData.birthday).getTime();
    var age_dt = new Date(month_diff);
    var year = age_dt.getUTCFullYear();
    var age = Math.abs(year - 1970);
    return age;
  }

  function getToday() {
    const date = new Date();
    return `${date.getFullYear()}-${
      date.getMonth() + 1 >= 10
        ? `${date.getMonth() + 1}`
        : `0${date.getMonth() + 1}`
    }-${date.getDate()}`;
  }

  function handleChange(e) {
    setChange(true);
    const { name, value } = e.target;
    setCurrentUserData((prev) => ({ ...prev, [name]: value }));
    if (name === "birthday") {
      if (calculateAge(value) < 13) setAgeError(true);
      else setAgeError(false);
    }

    if (name === "anniversary") {
      if (calculateAnniversary(value) < 18) setAnniversaryError(true);
      else setAnniversaryError(false);
    }
  }

  return (
    <>
      {!openEmailModal && !openPhoneModal && (
        <div className="relative w-full z-50">
          {/* Account Details */}
          <div className="w-6/12 mx-auto mt-2 bg-white rounded-md">
            <div className="w-full h-24 bg-gradient-to-r from-slate-900 to-rose-500 relative rounded-t-md">
              <div className="flex items-center gap-x-4 absolute top-4 left-10">
                {/* image circle */}
                <div className="w-24 h-24 rounded-full bg-slate-100 shadow-md flex items-center justify-center cursor-pointer">
                  <i>
                    <BsCamera size="30" />
                  </i>
                </div>
                <h1 className="font-bold text-white text-xl">
                  Hi,{" "}
                  {currentUserData.firstName
                    ? `${currentUserData.firstName} ${currentUserData.lastName}`
                    : "Guest"}
                </h1>
              </div>
            </div>

            <div className="px-20 py-10">
              <h1 className="font-bold text-lg">Account Details</h1>
              <div className="flex flex-col mt-4 gap-y-4">
                <div className="flex items-center gap-x-10">
                  <h1 className="text-md">Email Address: </h1>
                  <h1 className="text-sm">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    {currentUserData.email || "Add an email id"}
                  </h1>
                  <i
                    className="cursor-pointer hover:text-yellow-800"
                    onClick={() => setOpenEmailModal(true)}
                  >
                    <FiEdit />
                  </i>
                </div>
                <div className="flex items-center gap-x-10">
                  <h1 className="text-md">Mobile Number: </h1>
                  <h1 className="text-sm">+91 - {currentUserData.mobile}</h1>
                  <i
                    className="cursor-pointer hover:text-yellow-800"
                    onClick={() => setOpenPhoneModal(true)}
                  >
                    <FiEdit />
                  </i>
                </div>
              </div>
            </div>
          </div>

          {/* Personal Details */}
          <div className="w-6/12 mx-auto mt-2 bg-white rounded-md px-20 py-10">
            <h1 className="text-lg font-bold mb-10">Personal Details</h1>
            <div className="flex flex-col gap-y-8">
              <div className="flex justify-between items-center">
                <h1>First Name</h1>
                <input
                  type="text"
                  name="firstName"
                  value={currentUserData.firstName || ""}
                  onChange={handleChange}
                  className="border-[1px] border-black rounded-md w-96 h-8 mr-16 text-xs p-2"
                />
              </div>
              <div className="flex justify-between items-center">
                <h1>Last Name</h1>
                <input
                  type="text"
                  name="lastName"
                  value={currentUserData.lastName || ""}
                  onChange={handleChange}
                  className="border-[1px] border-black rounded-md w-96 h-8 mr-16 text-xs p-2"
                />
              </div>
              <div className="flex justify-between items-center">
                <h1>Birthday (Optional)</h1>
                <input
                  type="date"
                  name="birthday"
                  value={currentUserData.birthday || ""}
                  onChange={handleChange}
                  className="border-[1px] border-black rounded-md w-96 h-8 mr-16 text-xs p-2"
                  min="2000-01-01"
                  max={getToday()}
                />
              </div>
              {ageError ? (
                <h1 className="text-xs text-red-500 text-center">
                  Age should be atleast 13
                </h1>
              ) : (
                <></>
              )}
              <div className="flex items-center">
                <h1>Identity (Optional)</h1>
                <div className="flex ml-8 gap-x-4">
                  <button
                    className={`outline-none border-2 border-rose-400 px-4 py-1 rounded-md ${
                      currentUserData.gender == 1
                        ? "bg-rose-400 text-white"
                        : ""
                    }`}
                    onClick={() => {
                      setChange(true);
                      setCurrentUserData((prev) => ({ ...prev, gender: 1 }));
                    }}
                  >
                    Male
                  </button>
                  <button
                    className={`outline-none border-2 border-rose-400 px-4 py-1 rounded-md ${
                      currentUserData.gender == 0
                        ? "bg-rose-400 text-white"
                        : ""
                    }`}
                    onClick={() => {
                      setChange(true);
                      setCurrentUserData((prev) => ({ ...prev, gender: 0 }));
                    }}
                  >
                    Female
                  </button>
                </div>
              </div>
              <div className="flex items-center">
                <h1>Married? (Optional)</h1>
                <div className="flex ml-6 gap-x-4">
                  <button
                    className={`border-2 border-rose-400 px-4 py-1 rounded-md ${
                      currentUserData.married === 1
                        ? "bg-rose-400 text-white"
                        : ""
                    }`}
                    onClick={() => {
                      setChange(true);
                      setCurrentUserData((prev) => ({ ...prev, married: 1 }));
                    }}
                  >
                    Yes
                  </button>
                  <button
                    className={`border-2 border-rose-400 px-4 py-1 rounded-md ${
                      currentUserData.married === 0
                        ? "bg-rose-400 text-white"
                        : ""
                    }`}
                    onClick={() => {
                      setChange(true);
                      setCurrentUserData((prev) => ({
                        ...prev,
                        married: 0,
                        anniversary: "",
                      }));
                    }}
                  >
                    No
                  </button>
                </div>
              </div>
              <div>
                {currentUserData.married ? (
                  <>
                    <div className="flex justify-between items-center">
                      <h1>Anniversary (Optional)</h1>
                      <input
                        type="date"
                        name="anniversary"
                        value={currentUserData.anniversary || ""}
                        onChange={handleChange}
                        min="2000-01-01"
                        max={getToday()}
                        className="border-[1px] border-black rounded-md w-96 h-8 mr-16 text-xs p-2"
                      />
                    </div>
                    {anniversaryError ? (
                      <h1 className="text-xs text-red-500 text-center mt-4">
                        Age should be atleast 18 to be married
                      </h1>
                    ) : (
                      <></>
                    )}
                  </>
                ) : (
                  <></>
                )}
              </div>
            </div>
          </div>

          {/* Address (Optional) */}
          <div className="w-6/12 mx-auto mt-2 bg-white rounded-md px-20 py-10">
            <h1 className="text-lg font-bold mb-10">Address (Optional)</h1>
            <div className="flex flex-col gap-y-10">
              <div className="flex justify-between items-center">
                <h1>Address Line 1</h1>
                <input
                  type="text"
                  name="address1"
                  value={currentUserData.address1 || ""}
                  onChange={handleChange}
                  placeholder="Flat no., House no., Building,"
                  className="border-[1px] border-black rounded-md w-96 h-8 mr-16 text-xs p-2"
                />
              </div>
              <div className="flex justify-between items-center">
                <h1>Address Line 2</h1>
                <input
                  type="text"
                  name="address2"
                  value={currentUserData.address2 || ""}
                  onChange={handleChange}
                  placeholder="Area, Colony, Street, Sector, Village"
                  className="border-[1px] border-black rounded-md w-96 h-8 mr-16 text-xs p-2"
                />
              </div>
              <div className="flex justify-between items-center">
                <h1>Landmark</h1>
                <input
                  type="text"
                  name="landmark"
                  value={currentUserData.landmark || ""}
                  onChange={handleChange}
                  placeholder="E.g Prithvi Theater"
                  className="border-[1px] border-black rounded-md w-96 h-8 mr-16 text-xs p-2"
                />
              </div>
              <div className="flex justify-between items-center">
                <h1>State</h1>
                <select
                  name="state"
                  id="states"
                  onChange={handleChange}
                  className="border-[1px] border-black rounded-md w-96 h-10 mr-16 text-xs p-2"
                  value={currentUserData.state}
                >
                  {states.map((item, ind) => (
                    <option key={ind} value={item.state}>
                      {item.state}
                    </option>
                  ))}
                </select>
              </div>
              <div className="flex justify-between items-center">
                <h1>Town / City</h1>
                <select
                  name="city"
                  onChange={handleChange}
                  className="border-[1px] border-black rounded-md w-96 h-10 mr-16 text-xs p-2"
                  value={currentUserData.city}
                >
                  {dists?.map((item, ind) => (
                    <option key={ind} value={item}>
                      {item}
                    </option>
                  ))}
                </select>
              </div>
              <div className="flex justify-between items-center">
                <h1>Area Pincode</h1>
                <input
                  type="text"
                  name="pincode"
                  value={currentUserData.pincode || ""}
                  onChange={handleChange}
                  placeholder="E.g 560001"
                  className="border-[1px] border-black rounded-md w-96 h-8 mr-16 text-xs p-2"
                />
              </div>
            </div>
            <button
              className="fixed bottom-10 right-10 w-16 h-16 bg-rose-500 text-white rounded-full disabled:bg-slate-400 disabled:cursor-not-allowed disabled:text-black flex justify-center items-center"
              disabled={change && !ageError && !anniversaryError ? false : true}
              onClick={() => setUpdate(true)}
            >
              <i>
                <AiOutlineSave size={30} />
              </i>
            </button>
          </div>
        </div>
      )}
      <EmailModal />
      <PhoneModal />
    </>
  );
}
