import React, { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";

import { BsChevronDown } from "react-icons/bs";

function Payment() {
  const { id, hall, time, seats } = useParams();

  const [phone, setPhone] = useState(
    () => localStorage.getItem("current-user") || ""
  );
  const [currentUser, setCurrentUser] = useState({});
  const [movieData, setMovieData] = useState({});

  console.log(currentUser);

  useEffect(() => {
    const URL = `https://localhost:7285/User/GetUser?phone=${phone}`;
    fetch(URL)
      .then((res) => res.json())
      .then((data) => setCurrentUser(data))
      .catch((err) => console.log(err));
  }, []);

  useEffect(() => {
    const URL = `https://localhost:7285/Movie/GetMovie?id=${id}`;
    fetch(URL)
      .then((res) => res.json())
      .then((data) => setMovieData(data))
      .catch((err) => console.log(err));
  }, []);

  return (
    <div className="w-screen bg-slate-100 h-screen">
      <div className="w-full h-20 bg-slate-800 flex items-center">
        <Link to="/">
          <h1 className="text-[22px] text-white font-bold  ml-10">
            BookMyMovie
          </h1>
        </Link>
      </div>

      <div className="w-11/12 mt-4 mx-auto bg-slate-100 flex justify-between">
        {/* Part - 1 */}
        <div className="w-8/12 z-20">
          {/* Share your contact details */}
          <div className="mt-0">
            <div className="w-full h-16 bg-rose-500 flex items-center">
              <div className="flex items-center ml-4 gap-x-2 text-white">
                <i>
                  <BsChevronDown />
                </i>
                <h1>Share your contact details</h1>
              </div>
            </div>
            <div className="flex gap-x-4 w-full h-24 bg-slate-100 items-center pl-4 ">
              <input
                type="text"
                value={currentUser.email || ""}
                onChange={() => {}}
                className="w-[20rem] h-10 rounded-md focus:shadow-md px-4 text-sm outline-none"
                placeholder="Email Address"
              />
              <div className="relative">
                <input
                  type="text"
                  value={currentUser.mobile || ""}
                  onChange={() => {}}
                  className="w-[20rem] h-10 rounded-md focus:shadow-md pl-12 text-sm outline-none"
                  placeholder="Phone Number"
                />
                <h1 className="absolute top-2 left-3">+91</h1>
              </div>
              <button className="bg-rose-500 py-2 px-6 rounded-md text-white hover:bg-rose-800">
                Continue
              </button>
            </div>
          </div>

          {/* Unlock offers or Apply Promocodes */}
          <div className="w-full h-16 bg-gray-300 flex items-center mt-2">
            <div className="flex items-center ml-4 gap-x-2 text-black">
              <i>
                <BsChevronDown />
              </i>
              <h1>Unlock offers or Apply Promocodes</h1>
            </div>
          </div>

          {/* Payment Options */}
          <div className="mt-2">
            <div className="w-full h-16 bg-rose-500 flex items-center">
              <div className="flex items-center ml-4 gap-x-2 text-white">
                <i>
                  <BsChevronDown />
                </i>
                <h1>Payment Options</h1>
              </div>
            </div>

            <div className="w-full bg-white px-8 py-4">
              <h1 className="font-bold text-md text-black">Pay with UPI</h1>
              <div className="px-10 flex mt-4">
                <div className="flex-col">
                  <RadioButton
                    icon="https://assets-in.bmscdn.com/paymentcms/gpay.jpg"
                    name="Google Pay"
                  />
                  <RadioButton
                    icon="https://assets-in.bmscdn.com/paymentcms/bhim_web.png"
                    name="BHIM"
                  />
                  <RadioButton
                    icon="https://assets-in.bmscdn.com/paymentcms/phonepe_web.png"
                    name="PhonePe"
                  />
                </div>
                <div className="flex-col">
                  <RadioButton
                    icon="https://assets-in.bmscdn.com/paymentcms/amazonpayupi-v2.png"
                    name="Amazon Pay"
                  />
                  <RadioButton
                    icon="https://assets-in.bmscdn.com/paymentcms/paytmupi_web.png"
                    name="PayTM"
                  />
                  <RadioButton
                    icon="https://assets-in.bmscdn.com/paymentcms/OTHERUPI.png"
                    name="Other UPI"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Part - 2 */}
        <div className="w-[30%] bg-white relative">
          <div className="border-b-2 border-dashed relative px-4 py-6">
            <h1 className="font-thin text-md tracking-widest mb-10 ml-4 mt-4">
              ORDER SUMMARY
            </h1>
            <div className="px-4 relative">
              <div className="absolute -top-10 right-10 text-sm flex flex-col items-center -gap-y-4">
                <h1 className="font-bold">{seats.split(", ").length}</h1>
                <h1>Ticket{seats.split(", ").length > 1 ? "s" : ""}</h1>
              </div>
              {/* Order Summary */}
              <div className="flex gap-x-2">
                <h1 className="text-lg tracking-wide">{movieData.name}</h1>
                <h1 className="text-xs tracking-wide mt-2">
                  ({movieData.grade})
                </h1>
              </div>
              <h1 className="text-xs text-slate-500">
                {movieData.languages?.split(", ")[0]},{" "}
                {movieData.viewType?.split(", ")[0]}
              </h1>
              <h1 className="text-xs text-slate-500 mt-4">{hall}</h1>
              <h1 className="text-xs text-slate-500">Today, {time}</h1>
              <h1 className="text-xs text-slate-500 mt-1">
                {seats.split(", ").map((item, ind) => (
                  <i key={ind}>
                    {item}
                    {ind + 1 === seats?.split(", ").length ? "" : ", "}
                  </i>
                ))}
              </h1>
            </div>
            {/* hehe, My style */}
            <div className="w-6 h-6 rounded-full bg-slate-100 absolute -bottom-3 -right-3 z-10"></div>
            <div className="w-6 h-6 rounded-full bg-slate-100 absolute -bottom-3 -left-3 z-10"></div>
          </div>

          {/* part 2 */}
          <div className="h-full py-6 px-8">
            <div className="flex justify-between items-between">
              <h1 className="text-xs">Sub Total</h1>
              <h1 className="font-bold">
                Rs.{seats.split(", ").length * 100}.00
              </h1>
            </div>
            <div className="text-xs mt-4 flex justify-between">
              <h1>+ Convenience Fees</h1>
              <h1 className="font-light">+Rs. 30.00</h1>
            </div>
            <div className="absolute bottom-0 left-0 bg-yellow-100 w-full h-16 flex items-center justify-between text-sm font-bold px-10">
              <h1>Amount Payable</h1>
              <h1>Rs. {seats.split(", ").length * 100 + 30}.00</h1>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

const RadioButton = ({ icon, name }) => {
  return (
    <div className="flex items-center gap-x-4 hover:bg-slate-200 px-6 py-1 transition-all outline-none w-56 h-16 ">
      <input type="radio" name="payment" id={name} />
      <label htmlFor={name}>
        <div className="flex items-center gap-x-4 cursor-pointer">
          <img src={icon} className="w-10" />
          <h1 className="text-slate-500 text-xs">{name}</h1>
        </div>
      </label>
    </div>
  );
};

export default Payment;
