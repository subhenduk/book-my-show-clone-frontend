import React, { useContext, useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { AppContext } from "../Context";

import Header from "../components/Header";
import Navbar from "../components/Navbar";
import Modal from "../components/Modal";

export default function Theater() {
  const [movieDetails, setMovieDetails] = useState({});
  const [theaterDetails, setTheaterDetails] = useState([]);
  const { currentCity } = useContext(AppContext);
  const { id } = useParams();

  useEffect(() => {
    const URL = `https://localhost:7285/Movie/GetMovie?id=${id}`;
    fetch(URL)
      .then((res) => res.json())
      .then((data) => setMovieDetails(data))
      .catch((err) => console.log(err));
  }, [id]);

  useEffect(() => {
    const URL = `https://localhost:7285/Theater/GetTheater?currentCity=${currentCity}&currentMovie=${encodeURIComponent(
      movieDetails.name
    )}`;
    fetch(URL)
      .then((res) => res.json())
      .then((data) =>
        setTheaterDetails(
          data.map((item) => ({
            id: item.id,
            theater: item.theaters,
            timing: item.timings.split(", "),
          }))
        )
      )
      .catch((err) => console.log(err));
  }, [currentCity, movieDetails.name]);

  return (
    <div className="w-screen bg-slate-200">
      <Header />
      <Navbar />
      <Modal />

      <div className="w-full bg-slate-600">
        <div className="w-10/12 mx-auto h-36 flex flex-col justify-center">
          <h1 className="text-white font-bold text-[40px]">
            {movieDetails.name}
          </h1>
          <div className="flex gap-x-4">
            <div className="w-6 h-6 rounded-full bg-white flex justify-center items-center text-sm">
              <h1>{movieDetails.grade}</h1>
            </div>
            <h1 className="px-2 border rounded-full text-white text-sm">
              {movieDetails.genres}
            </h1>
          </div>
        </div>
      </div>

      <div className="w-10/12 mx-auto bg-white rounded-md">
        {theaterDetails.map((item) => (
          <div key={item.id} className="p-2">
            <div className="w-full font-bold h-20 flex items-center">
              <h1 className="w-[15%] text-center text">{item.theater}</h1>
              <div className="w-[85%] flex gap-x-2">
                {item.timing.map((time, ind) => (
                  <Link
                    to={`/movie/${movieDetails.id}/seatbooking/${item.theater}/${time}`}
                  >
                    <div key={ind}>
                      <div className="text-green-600 border-2 border-slate-400 px-4 py-2 rounded-md ">
                        {time}
                      </div>
                    </div>
                  </Link>
                ))}
              </div>
            </div>
            <hr />
          </div>
        ))}
      </div>
    </div>
  );
}
