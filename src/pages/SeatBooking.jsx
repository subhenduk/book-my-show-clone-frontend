import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import clsx from "clsx";

export default function SeatBooking({ rows = 8, cols = 8 }) {
  const [selectedSeats, setSelectedSeats] = useState([]);
  const [allSeats, setAllSeats] = useState(
    () => JSON.parse(localStorage.getItem("seats"))?.seats || 2
  );
  const [totalSeats, setTotalSeats] = useState(allSeats);
  const [givenSeats, setGivenSeats] = useState(0);

  // useEffect(() => {
  //   localStorage.setItem("seat-array", JSON.stringify(selectedSeats));
  // }, [selectedSeats]);

  const ROWS = rows;
  const COLS = cols;

  const seats = Array.from({ length: ROWS * COLS }, (_, i) => i);
  const index = Array.from({ length: ROWS }, (_, i) => i);

  const seatArray = Array.from(
    { length: totalSeats ? totalSeats : allSeats },
    (_, i) => i
  );

  const seatName = () => {
    const newArr = selectedSeats.map(
      (item) => `${String.fromCharCode(item / COLS + 65)}${(item % COLS) + 1}`
    );
    return newArr.join(", ");
  };

  const func = (seat) => {
    const newArr = seatArray
      .map((item) => {
        if ((seat % COLS) + item + 1 <= COLS) {
          return seat + item;
        }
      })
      .filter((item) => item);

    return newArr;
  };

  const handleSelectedState = (seat) => {
    const isSelected = selectedSeats.includes(seat);

    if (isSelected) {
      setSelectedSeats(
        selectedSeats.filter((selectedSeat) => selectedSeat !== seat)
      );
      setGivenSeats((prev) => prev - 1);
      setTotalSeats((prev) => prev + 1);
    } else {
      if (totalSeats !== 0) {
        setSelectedSeats([...selectedSeats, ...func(seat)]);
        setGivenSeats([...selectedSeats, ...func(seat)].length);
        setTotalSeats(allSeats - [...selectedSeats, ...func(seat)].length);
      } else {
        setSelectedSeats([]);
        setGivenSeats([...func(seat)].length);
        setTotalSeats(allSeats - [...func(seat)].length);
        setSelectedSeats([...func(seat)]);
      }
    }
  };

  return (
    <>
      <div className="flex">
        <div className="flex flex-col gap-y-[0.62rem] mr-6">
          {index.map((item, ind) => (
            <h1
              key={ind}
              className="w-8 h-8 flex justify-center items-center font-bold text-slate-400 select-none"
            >
              {String.fromCharCode(65 + item)}
            </h1>
          ))}
        </div>
        <div className="">
          <Seats cols={COLS}>
            {seats.map((seat) => {
              const isSelected = selectedSeats.includes(seat);
              return (
                <span
                  tabIndex="0"
                  key={seat}
                  className={clsx(
                    "w-8 h-8 rounded-sm border border-green-500  flex justify-center align-center cursor-pointer hover:bg-green-500 hover:text-white text-green-500 select-none",
                    isSelected && "bg-green-500 text-white"
                  )}
                  onClick={() => handleSelectedState(seat)}
                >
                  <p className="mt-[0.4rem] text-xs font-thin">
                    {(seat % COLS) + 1}
                  </p>
                </span>
              );
            })}
          </Seats>
        </div>
      </div>
      <Link to={`${seatName()}/payment`}>
        <button
          className="px-4 py-2 text-white bg-rose-600 rounded-md hover:bg-rose-800 shadow-md disabled:cursor-not-allowed disabled:bg-slate-400"
          disabled={totalSeats || false}
        >
          Book Ticket
        </button>
      </Link>
    </>
  );
}

const Seats = styled.div`
  display: grid;
  grid-gap: 10px;
  grid-template-columns: repeat(${(props) => props.cols || 8}, min-content);
  align-items: center;
`;
