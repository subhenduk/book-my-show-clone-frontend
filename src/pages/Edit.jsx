import React, { useContext } from "react";
import Drawer from "react-modern-drawer";
import "react-modern-drawer/dist/index.css";
import { AppContext } from "../Context";

import Header from "../components/Header";
import Navbar from "../components/Navbar";
// import SignIn from "../components/SignIn";
// import Modal from "../components/Modal";
import EditNav from "../components/EditNav";
import DrawerItems from "../components/DrawerItems";
import EditPage from "../components/EditPage";

export default function Edit() {
  const { drawer, setDrawer } = useContext(AppContext);

  return (
    <div className="relative w-screen bg-slate-200">
      <Header />
      <Navbar />
      {/* <SignIn /> */}
      {/* <Modal /> */}
      <EditNav />
      <EditPage />
      <Drawer
        open={drawer}
        onClose={() => setDrawer((prev) => !prev)}
        direction="right"
        size="350px"
        className=""
      >
        <DrawerItems />
      </Drawer>
    </div>
  );
}
