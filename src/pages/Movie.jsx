import React, { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";

import { BsStarFill, BsChevronRight } from "react-icons/bs";

import Header from "../components/Header";
import Navbar from "../components/Navbar";
import Modal from "../components/Modal";

export default function Movie() {
  const [movieData, setMovieData] = useState({});
  const { id } = useParams();

  useEffect(() => {
    const URL = `https://localhost:7285/Movie/GetMovie?id=${id}`;
    fetch(URL)
      .then((res) => res.json())
      .then((data) => setMovieData(data))
      .catch((err) => console.log(err));
  }, []);

  return (
    <>
      <Header />
      <Navbar />
      <Modal />
      <div
        className="w-full h-[30rem] bg-no-repeat bg-right"
        style={{
          backgroundImage: `linear-gradient(90deg, #1A1A1A 24.97%, #1A1A1A 38.3%, rgba(26, 26, 26, 0.0409746) 97.47%, #1A1A1A 100%), url(${movieData.background})`,
        }}
      >
        <div className="h-full flex items-center mx-36">
          {/* Movie cover */}
          <img src={movieData.cover} className="rounded-xl" />
          <div className="flex flex-col justify-start gap-y-4 ml-10">
            {/* name */}
            <h1 className="text-[40px] font-bold text-white">
              {movieData.name}
            </h1>
            {/* rating and voting */}
            <div className="flex items-center gap-x-2">
              <i className="text-rose-500">
                <BsStarFill size="32" />
              </i>
              <h1 className="text-white font-bold text-[30px]">
                {movieData.rating}/10
              </h1>
              <div className="text-white text-sm mt-2 flex items-center">
                <h1 className="text-white text-sm">{movieData.votes}K votes</h1>
                <i className="mt-1 ml-1">
                  <BsChevronRight size={10} />
                </i>
              </div>
            </div>
            <div className="bg-zinc-700 bg-opacity-80 rounded-xl px-6 py-2 flex items-center justify-between">
              {/*  */}
              <div>
                <h1 className="text-white text-lg">Add your rating & review</h1>
                <h1 className="text-slate-300 text-md">Your rating matter</h1>
              </div>
              <button className="px-4 py-2 bg-white rounded-md ml-5">
                Rate Now
              </button>
            </div>

            {/* watch type and genre */}
            <div className="flex gap-x-2">
              <div className="bg-white px-2 py-1 rounded-md cursor-pointer">
                {movieData.viewType}
              </div>
              <div className="bg-white px-2 py-1 rounded-md cursor-pointer">
                {movieData.languages}
              </div>
            </div>

            {/*  */}
            <div className="flex gap-x-1 text-white">
              <h1>{movieData.movieLength}</h1>
              <p>•</p>
              <h1 className="hover:underline cursor-pointer">
                {movieData.genres}
              </h1>
              <p>•</p>
              <h1>{movieData.grade}</h1>
              <p>•</p>
              <h1>{movieData.releaseDate}</h1>
            </div>

            <div>
              <Link to={`/movie/${id}/bookticket`}>
                <button className="bg-rose-500 px-4 py-2 text-white font-bold rounded-md hover:bg-rose-800">
                  Book Tickets
                </button>
              </Link>
            </div>
          </div>
        </div>

        <div className="w-10/12 mx-auto my-10">
          <h1 className="text-[20px] font-bold">About the movie</h1>
          <p className="mt-4">{movieData.aboutMovie}</p>
        </div>
      </div>
    </>
  );
}
