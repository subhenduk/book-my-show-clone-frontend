import React, { useContext } from "react";
import Drawer from "react-modern-drawer";
import "react-modern-drawer/dist/index.css";

import Header from "../components/Header";
import Navbar from "../components/Navbar";
import Modal from "../components/Modal";
import SignIn from "../components/SignIn";
import Recommended from "../components/Recommended";
import DrawerItems from "../components/DrawerItems";
import Slider from "../components/Slider";

import { AppContext } from "../Context";

export default function Home() {
  const { drawer, setDrawer } = useContext(AppContext);

  return (
    <div className="relative w-full overflow-y-hidden">
      <Header />
      <Navbar />
      <Modal />
      <SignIn />
      <Slider />
      <Drawer
        open={drawer}
        onClose={() => setDrawer((prev) => !prev)}
        direction="right"
        size="350px"
        className=""
      >
        <DrawerItems />
      </Drawer>
      <div className="w-10/12 mx-auto">
        <Recommended />
      </div>
    </div>
  );
}
