import React, { useEffect, useState, useContext } from "react";
import { useParams, Link } from "react-router-dom";

// icons
import { AiOutlineEdit, AiTwotoneHome } from "react-icons/ai";

import SeatBooking from "./SeatBooking";
import SeatModal from "../components/SeatModal";
import { AppContext } from "../Context";

export const TheaterContext = React.createContext();

export default function Theater() {
  const [seats, setSeats] = useState(
    JSON.parse(localStorage.getItem("seats"))?.seats || 2
  );
  const { id, hall, time } = useParams();
  const [movieData, setMovieData] = useState({});

  const { seatModal, setSeatModal } = useContext(AppContext);

  useEffect(() => {
    const URL = `https://localhost:7285/Movie/GetMovie?id=${id}`;
    fetch(URL)
      .then((res) => res.json())
      .then((data) => setMovieData(data))
      .catch((err) => console.log(err));
  }, []);

  return (
    <>
      <div className="w-full h-20 bg-slate-900 flex justify-between items-center">
        <div className="flex items-center ml-5 gap-x-5">
          <Link to="/">
            <i className="text-white cursor-pointer">
              <AiTwotoneHome size={25} />
            </i>
          </Link>
          <div>
            <div className="flex items-center gap-x-4">
              <h1 className="text-xl text-white font-bold">{movieData.name}</h1>
              <p className="w-6 h-6 border border-white rounded-full text-xs text-white flex items-center justify-center">
                {movieData.grade}
              </p>
            </div>
            <div className="flex">
              <h1 className="text-slate-400 text-xs font-semibold">
                {hall}, Today {time}
              </h1>
            </div>
          </div>
        </div>

        <div
          className="flex cursor-pointer mr-10 items-center border bordre-white rounded-md"
          onClick={() => setSeatModal(true)}
        >
          <h1 className="text-sm text-white pl-2 pr-1 py-1">{seats} tickets</h1>
          <i className="text-white text-xl cursor-pointer pr-2">
            <AiOutlineEdit />
          </i>
        </div>
      </div>
      {seatModal ? (
        <SeatModal />
      ) : (
        <>
          <div className="Cinema w-screen mt-24 flex flex-col gap-y-6 items-center justify-center">
            <h1 className="text-xs text-slate-500">
              All eyes this way please!
            </h1>
            <div className="screen mb-10"></div>
            <SeatBooking rows={6} cols={16} />
          </div>
        </>
      )}
    </>
  );
}
