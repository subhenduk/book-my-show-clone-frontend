import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import Home from "./pages/Home";
import Edit from "./pages/Edit";
import Movie from "./pages/Movie";
import Theaters from "./pages/Theaters";
import Theater from "./pages/Theater";
import SeatBooking from "./pages/SeatBooking";
import Payment from "./pages/Payment";

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" exact element={<Home />} />
        <Route path="/edit" element={<Edit />} />
        <Route path="/movie/:id" element={<Movie />} />
        <Route path="/movie/:id/bookticket" exact element={<Theaters />} />
        <Route
          path="/movie/:id/seatbooking/:hall/:time"
          element={<Theater />}
        />
        <Route
          path="/movie/:id/seatbooking/:hall/:time/:seats/payment"
          exact
          element={<Payment />}
        />
      </Routes>
    </Router>
  );
}

export default App;
